﻿
using Challenge.Controls;
using Challenge.Droid.Services;
using Firebase.Iid;
using Xamarin.Forms;

[assembly: Dependency(typeof(FireBaseToken))]
namespace Challenge.Droid.Services
{
    class FireBaseToken : IFireBaseControls
    {
        public string GetToken()
        {
            string refreshedToken = FirebaseInstanceId.Instance.Token;

            return refreshedToken;
        }
    }
}