﻿using System;
using Android.App;
using Android.Content;
using Android.Media;
using Android.Util;
using Firebase.Messaging;
using System.Collections.Generic;
using Android.Support.V4.App;
using Android.OS;

namespace Challenge.Droid
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class MyFirebaseMessagingService : FirebaseMessagingService
    {
        const string TAG = "MyFirebaseMsgService";
        public override void OnMessageReceived(RemoteMessage message)
        {
            Log.Debug(TAG, "From: " + message.From);

            var body = message.GetNotification().Body;
            Log.Debug(TAG, "Notification Message Body: " + body);
            SendNotification(message.GetNotification(), message.Data);
        }

        private void SendNotification(String messageBody, String messageTitle)
        {
            /*var intent = new Intent(this, typeof(MainActivity)); intent.AddFlags(ActivityFlags.ClearTop);

            int oneTimeID = (int)SystemClock.UptimeMillis();

            var pendingIntent = PendingIntent.GetActivity(this, oneTimeID, intent, PendingIntentFlags.OneShot);
            var notificationBuilder = new NotificationCompat.Builder(this, MainActivity.CHANNEL_ID).SetSmallIcon(Resource.Drawable.user).SetContentTitle(messageTitle).SetContentText(messageBody).SetAutoCancel(true).SetContentIntent(null);
            var notificationManager = NotificationManagerCompat.From(this); notificationManager.Notify(oneTimeID, notificationBuilder.Build());*/
        }

        private void SendNotification(RemoteMessage.Notification messageBody, IDictionary<string, string> data)
        {
            /*var intent = new Intent(this, typeof(MainActivity)); intent.AddFlags(ActivityFlags.ClearTop);
            foreach (var key in data.Keys)
            {
                intent.PutExtra(key, data[key]);
            }
            var pendingIntent = PendingIntent.GetActivity(this, MainActivity.NOTIFICATION_ID, intent, PendingIntentFlags.OneShot);
            var notificationBuilder = new NotificationCompat.Builder(this, MainActivity.CHANNEL_ID).SetSmallIcon(Resource.Drawable.user).SetContentTitle(messageBody.Title).SetContentText(messageBody.Body).SetAutoCancel(true).SetContentIntent(null);
            var notificationManager = NotificationManagerCompat.From(this); notificationManager.Notify(MainActivity.NOTIFICATION_ID, notificationBuilder.Build());*/
        }

    }

 
}