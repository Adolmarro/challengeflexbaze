﻿using Challenge.Apps.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Challenge.Components
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActionItemView : StackLayout
    {
        private BestMatches _model;
        public ActionItemView()
        {
            InitializeComponent();
        }

        public BestMatches Model
        {
            set
            {
                _model = value;
                Name.Text = value.Name.ToString();
                Symbol.Text = value.Symbol.ToString();
                Currency.Text = value.Currency.ToString();
                Region.Text = value.Region.ToString();

            }
        }
    }
}