﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Controls
{
    public interface IFireBaseControls
    {
        string GetToken();
    }
}
