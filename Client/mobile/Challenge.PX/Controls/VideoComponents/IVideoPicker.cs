﻿using System;
using System.Threading.Tasks;

namespace Challenge.Controls.VideoComponents
{
    public interface IVideoPicker
    {
        Task<string> GetVideoFileAsync();
    }
}
