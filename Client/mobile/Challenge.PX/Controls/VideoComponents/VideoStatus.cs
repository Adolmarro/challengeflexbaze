﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Controls.VideoComponents
{
    public enum VideoStatus
    {
        NotReady,
        Playing,
        Paused
    }
}
