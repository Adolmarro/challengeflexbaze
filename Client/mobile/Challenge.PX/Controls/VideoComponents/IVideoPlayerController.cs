﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Controls.VideoComponents
{
    public interface IVideoPlayerController
    {
        VideoStatus Status { set; get; }

        TimeSpan Duration { set; get; }
    }
}
