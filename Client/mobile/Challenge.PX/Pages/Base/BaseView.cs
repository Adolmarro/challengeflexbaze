﻿using Challenge.Apps.Core.ViewModels.Base;
using Challenge.Controls;
using Microsoft.AppCenter.Crashes;
using MvvmCross.Forms.Views;
using Plugin.Toast;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Challenge.Pages.Base
{
    public class BaseView<TViewModel> : MvxContentPage<TViewModel> where TViewModel : BaseViewModel
    {
        public BaseView()
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;
        }

        private void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            //var newExc = new Exception("TaskSchedulerOnUnobservedTaskException", e.Exception);
            //Crashes.TrackError(newExc);
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            //var newExc = new Exception("CurrentDomainOnUnhandledException", e.ExceptionObject as Exception);
            //Crashes.TrackError(newExc);
        }

        public new TViewModel ViewModel
        {
            get
            {
                var viewModel = (TViewModel)base.ViewModel;
                return viewModel;
            }
            set
            {
                base.ViewModel = value;
            }
        }
        protected override void OnViewModelSet()
        {
            ViewModel.OnShowMessage.Requested += OnShowMessage_Requested;
            ViewModel.OnShowProgressDialog.Requested += OnShowProgressDialog_Requested;
            ViewModel.OnCloseProgressDialog.Requested += OnCloseProgressDialog_Requested;
            ViewModel.OnShowToast.Requested += ShowToast_Requested;
        }

        private void ShowToast_Requested(object sender, MvvmCross.Base.MvxValueEventArgs<string> e)
        {
            CrossToastPopUp.Current.ShowCustomToast(e.Value, "#ebebeb", "#000000", Plugin.Toast.Abstractions.ToastLength.Long);
            //DependencyService.Get<IMessage>().LongAlert(e.Value);
        }


        /// <summary>
        /// Open a progress dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnCloseProgressDialog_Requested(object sender, EventArgs e)
        {
            try
            {
                await PopupNavigation.PopAsync();
            }
            catch (Exception ex) {
                Crashes.TrackError(ex);
            }
        }
        /// <summary>
        /// Close a progress dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnShowProgressDialog_Requested(object sender, EventArgs e)
        {
            await PopupNavigation.PushAsync(new LoadingPage());
        }
        //Show a alert whit a message
        private async void OnShowMessage_Requested(object sender, MvvmCross.Base.MvxValueEventArgs<string> e)
        {
            await DisplayAlert("Info",e.Value,"Cancel");
        }
    }
}
