﻿using Challenge.Apps.Core.ViewModels.Challenge;
using Challenge.Pages.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Challenge.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SelectedItem : BaseView<SelectedItemViewModel>
    {
        public SelectedItem()
        {
            InitializeComponent();
        }
    }
}