﻿using Challenge.Apps.Core.ViewModels.Challenge;
using MvvmCross.IoC;
using MvvmCross.ViewModels;

namespace Challenge.Apps.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Repository")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            RegisterAppStart<SearchViewModel>();

        }
    }
}
