﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Apps.Core.Http
{
    public class LoginDto
    {
        public string LoginId { get; set;  }
        public string Password { get; set; }
        public DateTime LastLoginDate { get; set; }
    }
}
