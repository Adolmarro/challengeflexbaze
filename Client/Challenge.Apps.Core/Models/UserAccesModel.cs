﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Apps.Core.Models
{
    public class UserAccesModel
    {
        public string CacheToken { get; set; }
        public int EntityUserId { get; set; }

        public int Id { get; set; }

        public string LoginId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int EmployeeId { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastUpdatedDate { get; set; }

        public DateTime? LastLoginDate { get; set; }

        public int LoginCount { get; set; }

        public bool IsServiceAccount { get; set; }

        public bool IsAccountLocked { get; set; }
        public string AccountLockedReason { get; set; }
        public bool IsPendingEmailValidation { get; set; }
        public bool PasswordResetRequired { get; set; }

        public DateTime? LastInvalidLoginDate { get; set; }
        public DateTime? PasswordResetTempPasswordTimeoutDate { get; set; }
        public DateTime? PasswordLastResetDate { get; set; }
        public int InvalidLoginCount { get; set; }

        public List<int> RoleIds { get; set; }
        public List<string> RoleNames { get; set; }
        public List<string> ResourceNames { get; set; }
    }
}
