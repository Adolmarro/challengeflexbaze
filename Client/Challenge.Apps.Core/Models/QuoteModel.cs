﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Apps.Core.Models
{
    public class QuoteModel
    {
        [JsonProperty("Global Quote")]
        public GlobalQuoteModel GlobalQuote { get; set; }
    }

    public class GlobalQuoteModel 
    {
        [JsonProperty("01. symbol")]
        public string Symbol { get; set; } = "";

        [JsonProperty("02. open")]
        public string Open { get; set; } = "";

        [JsonProperty("03. high")]
        public string High { get; set; } = "";

        [JsonProperty("04. low")]
        public string Low { get; set; } = "";

        [JsonProperty("05. price")]
        public string Price { get; set; } = "";

        [JsonProperty("06. volume")]
        public string Volume { get; set; } = "";

        [JsonProperty("07. latest trading day")]
        public string Latest_Trading_Day { get; set; } = "";

        [JsonProperty("08. previous close")]
        public string Previous_Close { get; set; } = "";

        [JsonProperty("09. change")]
        public string Change { get; set; } = "";

        [JsonProperty("10. change percent")]
        public string Change_Percent { get; set; } = "";
    }
}
