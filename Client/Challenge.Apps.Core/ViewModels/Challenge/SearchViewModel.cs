﻿using Challenge.Apps.Core.Models;
using Challenge.Apps.Core.Repository.Interfaces;
using Challenge.Apps.Core.Services.Implementations;
using Challenge.Apps.Core.ViewModels.Base;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Apps.Core.ViewModels.Challenge
{
    public class SearchViewModel : BaseViewModel
    {
        #region Fields
        IFinancialMarketRepository _financialMarketRepository;
        MvxObservableCollection<BestMatches> _bestMatches = new MvxObservableCollection<BestMatches>();
        #endregion

        #region Getters
        public MvxObservableCollection<BestMatches> BestMatches
        {
            get
            {
                return _bestMatches;
            }
            set
            {
                _bestMatches = value;
                RaisePropertyChanged(() => BestMatches);
            }
        }

        string _search = "";
        public string Search
        {
            get
            {
                return _search;
            }
            set
            {
                _search = value;
                RaisePropertyChanged(() => Search);
            }
        }

        bool _isEmpty = false;
        public bool IsEmpty
        {
            get
            {
                return _isEmpty;
            }
            set
            {
                _isEmpty = value;
                RaisePropertyChanged(() => IsEmpty);
            }
        }

        #endregion

        #region Methods
        public SearchViewModel(IMvxNavigationService navigationService, IFinancialMarketRepository financialMarketRepository) : base(navigationService)
        {
            _financialMarketRepository = financialMarketRepository;
            _financialMarketRepository.Error.Requested += Error_Requested;
        }

        public override void Prepare()
        {
            base.Prepare();
        }

        private async Task GetSearch() 
        {
            var response = await GetApiCall(new MakeCallTwoParams<SearchModel, string, string>(_financialMarketRepository.Search), _search , Configuration.ApiKey);

            if (response.BestMatches != null)
            {
                if (response.BestMatches.Count == 0)
                {
                    BestMatches = new MvxObservableCollection<BestMatches>();
                    IsEmpty = true;
                }
                else 
                {
                    BestMatches = response.BestMatches;
                    IsEmpty = false;
                }
                
            }
            else 
            {
                BestMatches = new MvxObservableCollection<BestMatches>();
                IsEmpty = true;
            }
        }

        public async void onItemSelected(BestMatches value)
        {
           await navigationService.Navigate<SelectedItemViewModel, object, object>(value);
        }
        #endregion

        #region Commands
        private IMvxAsyncCommand searchCommand;
        public IMvxAsyncCommand SearchCommand => searchCommand = searchCommand ?? new MvxAsyncCommand(async () => await GetSearch());

        private IMvxCommand selectedCommand;
        public IMvxCommand SelectedCommand => selectedCommand = selectedCommand ?? new MvxCommand<BestMatches>(onItemSelected);
        #endregion
    }
}
