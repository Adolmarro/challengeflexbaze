﻿using Challenge.Apps.Core.Models;
using Challenge.Apps.Core.Repository.Interfaces;
using Challenge.Apps.Core.Services.Implementations;
using Challenge.Apps.Core.ViewModels.Base;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Apps.Core.ViewModels.Challenge
{
    public class SelectedItemViewModel : BaseViewModel
    {
        #region Fields
        IFinancialMarketRepository _financialMarketRepository;
        BestMatches _bestMatche;


        GlobalQuoteModel _globalQuote;
        public GlobalQuoteModel GlobalQuote
        {
            get
            {
                return _globalQuote;
            }
            set
            {
                _globalQuote = value;
                RaisePropertyChanged(() => Price);
                RaisePropertyChanged(() => GlobalQuote);
                RaisePropertyChanged(() => Change);
                RaisePropertyChanged(() => Change_Percent);
                RaisePropertyChanged(() => Open);
                RaisePropertyChanged(() => Previous_Close);
                RaisePropertyChanged(() => Arrow);
            }
        }
        #endregion

        #region Getters
        public double Price
        {
            get
            {
                return Math.Truncate(Convert.ToDouble(_globalQuote.Price) * 100) / 100;
            }
        }

        public string Change
        {
            get
            {
                string op = "+";
                double value = Math.Truncate(Convert.ToDouble(_globalQuote.Change) * 100) / 100;

                if (value < 0) 
                {
                    op = "";
                }
                return op + value;
            }
        }

        public string Change_Percent
        {
            get
            {
                string value = _globalQuote.Change_Percent.Replace("%", "");

                string op = "+";
                double value_change = Math.Truncate(Convert.ToDouble(_globalQuote.Change) * 100) / 100;

                if (value_change < 0)
                {
                    op = "";
                }

                return op + Math.Truncate(Convert.ToDouble(value) * 100) / 100;
            }
        }

        public double Open
        {
            get
            {
                return Math.Truncate(Convert.ToDouble(_globalQuote.Open) * 100) / 100;
            }
        }

        public double Previous_Close
        {
            get
            {
                return Math.Truncate(Convert.ToDouble(_globalQuote.Previous_Close) * 100) / 100;
            }
        }

        public string Arrow
        {
            get 
            {
                double value = Math.Truncate(Convert.ToDouble(_globalQuote.Change) * 100) / 100;
                if (value < 0) 
                {
                    return "down_arrow.png";
                }
                else 
                {
                    return "up_arrow.png";
                }
            }
        }
        #endregion

        #region Methods
        public SelectedItemViewModel(IMvxNavigationService navigationService, IFinancialMarketRepository financialMarketRepository) : base(navigationService)
        {
            _financialMarketRepository = financialMarketRepository;
            _financialMarketRepository.Error.Requested += Error_Requested;
        }

        public override async void Prepare(object value)
        {
            base.Prepare();
            _bestMatche = (BestMatches)value;
            IsLoading = true;
            await GetQuote();
        }

        private async Task GetQuote()
        {
            var response = await GetApiCall(new MakeCallTwoParams<QuoteModel, string, string>(_financialMarketRepository.Quotes), _bestMatche.Symbol, Configuration.ApiKey);

            if (response != null)
            {
                GlobalQuote = response.GlobalQuote;
                IsLoading = false;
            }
            else
            {
                GlobalQuote = new GlobalQuoteModel();
            }


        }
        #endregion
    }
}
