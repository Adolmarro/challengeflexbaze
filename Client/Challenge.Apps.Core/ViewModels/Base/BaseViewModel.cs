﻿using Challenge.Apps.Core.Http;
using Challenge.Apps.Core.Repository.Base.Implementations;
using Challenge.Apps.Core.Repository.Base.Interfaces;
using Challenge.Apps.Core.Resources;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using Newtonsoft.Json;
using Refit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Apps.Core.ViewModels.Base
{
    public delegate Task<TResponse> MakeCallOneParam<TResponse, TParam>(TParam param);
    public delegate Task<TResponse> MakeCall<TResponse>();
    public delegate Task<TResponse> MakeCallTwoParams<TResponse, TParam1, TParam2>(TParam1 param, TParam2 param2);
    public class BaseViewModel : MvxViewModel<object, object>
    {
        #region Fields
        protected readonly IMvxNavigationService navigationService;
        private MvxInteraction<string> _showMessage = new MvxInteraction<string>();
        private MvxInteraction<string> _showToast = new MvxInteraction<string>();
        private MvxInteraction _closeSession = new MvxInteraction();
        private MvxInteraction _showProgressDialog = new MvxInteraction();
        private MvxInteraction _closeProgressDialog = new MvxInteraction();
        private string title;
        private bool isloading = true;
        #endregion

        #region Getters
        public IMvxInteraction<string> OnShowMessage => _showMessage;
        public IMvxInteraction<string> OnShowToast => _showToast;
        public IMvxInteraction CloseSession => _closeSession ;
        public IMvxInteraction OnShowProgressDialog => _showProgressDialog ;
        public IMvxInteraction OnCloseProgressDialog => _closeProgressDialog ;
        public string this[string index, string P] =>( P == "TX")? TXStrings.ResourceManager.GetString(index) : PXStrings.ResourceManager.GetString(index);
        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
                RaisePropertyChanged(() => Title);
            }
        }

        public bool IsLoading 
        {
            get
            {
                return isloading;
            }
            set
            {
                isloading = value;
                RaisePropertyChanged(() => IsLoading);
            }
        }
        #endregion

        #region Constructors
        public BaseViewModel(IMvxNavigationService navigationService)
        {
            this.navigationService = navigationService;
        }
        #endregion

        #region Methods
        public async Task<TResponse> GetApiCall<TResponse>(MakeCall<TResponse> makeCall)
        {
            ShowDialog();
            var response = await makeCall();
            CloseDialog();
            return response;
        }
        public async Task<TResponse> GetApiCall<TResponse, TParam>(MakeCallOneParam<TResponse, TParam> makeCall, TParam param)
        {
            ShowDialog();
            var response = await makeCall(param);
            CloseDialog();
            return response;
        }
        public async Task<TResponse> GetApiCall<TResponse, TParam1, TParam2>(MakeCallTwoParams<TResponse, TParam1, TParam2> makeCall, TParam1 param1, TParam2 param2)
        {
            ShowDialog();
            var response = await makeCall(param1, param2);
            CloseDialog();
            return response;
        }
        public override void Prepare(object parameter)
        {

        }
        protected async void Error_Requested(object sender, MvvmCross.Base.MvxValueEventArgs<Exception> e)
        {
            await ErrorHandle(e.Value);
        }
        public async Task ErrorHandle(Exception e)
        {
            string message;
            if (e is ApiException apiException)
            {
                if (apiException.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    _closeSession.Raise();
                    message = (await apiException.GetContentAsAsync<Dictionary<string, string>>())["message"];
                }
                else
                { 
                    if (apiException.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        message = apiException.Message;
                    }
                    else
                    { 
                        message= (await apiException.GetContentAsAsync<Dictionary<string, string>>())["message"];
                    }
                }
                ShowToast(message);
            }
            else
            {
                ShowToast(e.Message);
            }
        }
        public void ShowMessage(string message)
        {
            _showMessage.Raise(message);
        }

        public void ShowToast(string message)
        {
            _showToast.Raise(message);
        }

        public void ShowDialog()
        {
            _showProgressDialog.Raise();
        }

        public void CloseDialog()
        {
            _closeProgressDialog.Raise();
        }
        #endregion
    }
}