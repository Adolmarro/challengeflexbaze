﻿using Akavache;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Linq;

namespace Challenge.Apps.Core.Helpers
{
    public class CacheHelper
    {

        public static async void AddObjectInMemory<T>(string key, T _object)
        {
            await BlobCache.InMemory.InsertObject<T>(key, _object);
        }
        public static async void AddObjectInUserAccount<T>(string key, T _object)
        {
            await BlobCache.UserAccount.InsertObject<T>(key, _object);
        }
        public static async Task AddObjectInLocalMachine<T>(string key, T _object)
        {
            await BlobCache.LocalMachine.InsertObject<T>(key, _object);
        }
        public static async Task AddObjectInSecure<T>(string key, T _object)
        {
            await BlobCache.Secure.InsertObject<T>(key, _object);
        }
        public static void RemoveObjectFromMemory(string key)
        {
            try
            {
                BlobCache.InMemory.Invalidate(key);
            }
            catch (KeyNotFoundException e)
            {
                throw e;
            }
        }
        public static void RemoveObjectFromUserAccount(string key)
        {
            try
            {
                BlobCache.UserAccount.Invalidate(key);
            }
            catch (KeyNotFoundException e)
            {
                throw e;
            }
        }
        public static bool RemoveObjectFromLocalMachine(string key)
        {
            bool response = true;
            try
            {
                BlobCache.LocalMachine.Invalidate(key);
            }
            catch (KeyNotFoundException e)
            {
                Console.WriteLine(e);
                response = false;
            }

            return response;
        }
        public static void RemoveObjectFromSecure(string key)
        {
            try
            {
                BlobCache.Secure.Invalidate(key);
            }
            catch (KeyNotFoundException e)
            {
                throw e;
            }
        }
        public static async Task<T> GetObjectFromLocalMachine<T>(string key) where T : class
        {
            try
            {
                return await BlobCache.LocalMachine.GetObject<T>(key);
            }
            catch (KeyNotFoundException e)
            {
                return null;
            }
        }

        public static async Task<T> GetObjectFromSecure<T>(string key) where T: class
        {
            try
            {
                return await BlobCache.Secure.GetObject<T>(key);
            }
            catch (KeyNotFoundException e)
            {
                return null;
            }
        }

        public static async void RemoveLocalMachineCacheData()
        {
            await BlobCache.LocalMachine.InvalidateAll();
        }

        public static async void RemoveSecureCacheData()
        {
            await BlobCache.Secure.InvalidateAll();
        }

    }
}
