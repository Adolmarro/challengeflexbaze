﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Apps.Core.Enums
{
    public enum MessageMode
    {
        Text,
        Audio,
        Video,
        Document
    }
}
