﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Apps.Core.Enums
{
    public enum NotificationType
    {
        Push = 1,
        Email
    }
}
