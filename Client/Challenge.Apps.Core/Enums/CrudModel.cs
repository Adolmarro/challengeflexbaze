﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Apps.Core.Enums
{
    public enum CrudMode
    {
        Edit,
        Create
    }
}
