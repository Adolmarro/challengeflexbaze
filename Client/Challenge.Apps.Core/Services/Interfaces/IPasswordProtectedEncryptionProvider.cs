﻿using Akavache;
using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Apps.Core.Services.Interfaces
{
    public interface IPasswordProtectedEncryptionProvider : IEncryptionProvider
    {
        void SetPassword(string password);
    }
}
