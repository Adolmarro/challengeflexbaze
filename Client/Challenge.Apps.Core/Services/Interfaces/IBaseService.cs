﻿using Challenge.Apps.Core.Http;
using Challenge.Apps.Core.Models;
using Refit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Challenge.Apps.Core.Services.Interfaces
{
    [Headers("Content-Type: application/json")]
    public interface IBaseService
    {
        [Post("/api/userAccess/login")]
        Task<UserAccesModel> Login([Body]LoginDto loginDto, CancellationToken cancellationToken);
    }
}
