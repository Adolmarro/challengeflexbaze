﻿using Challenge.Apps.Core.Http;
using Challenge.Apps.Core.Models;
using Refit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Challenge.Apps.Core.Services.Interfaces
{
    [Headers("Content-Type: application/json")]
    public interface IFinancialMarketService
    {
        [Get("/query?function=SYMBOL_SEARCH&keywords={keywords}&apikey={apikey}")]
        Task<SearchModel> Search(string keywords, string apikey, CancellationToken cancellationToken);

        [Get("/query?function=GLOBAL_QUOTE&symbol={symbol}&apikey={apikey}")]
        Task<QuoteModel> Quotes(string symbol, string apikey, CancellationToken cancellationToken);

    }
}
