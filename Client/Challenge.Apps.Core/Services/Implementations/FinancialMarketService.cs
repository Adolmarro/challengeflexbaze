﻿using Challenge.Apps.Core.Http;
using Challenge.Apps.Core.Models;
using Challenge.Apps.Core.Services.Interfaces;
using Refit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Challenge.Apps.Core.Services.Implementations
{
    class FinancialMarketService : ServiceRestBase<IFinancialMarketService>, IFinancialMarketService
    {
        public Task<QuoteModel> Quotes(string symbol, string apikey, CancellationToken cancellationToken)
        {
            return m_service.Quotes(symbol, apikey, cancellationToken);
        }

        public Task<SearchModel> Search(string keywords, string apikey, CancellationToken cancellationToken)
        {
            return m_service.Search(keywords, apikey, cancellationToken);
        }
    }
}
