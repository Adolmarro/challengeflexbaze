﻿using Akavache;
using Challenge.Apps.Core.Helpers;
using Challenge.Apps.Core.Http;
using Challenge.Apps.Core.Models;
using MvvmCross.ViewModels;
using Splat;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Apps.Core.Services.Implementations
{
    public class Configuration
    {
        private static MvxInteraction<bool> _readyForConversation= new MvxInteraction<bool>();
        private static MvxInteraction<string> _onTokenChange = new MvxInteraction<string>();

        public  static void Initialize()
        {
            
        }
        public static string BaseUrlQA { get; set; } = "https://www.alphavantage.co/";

        public static string BaseUrlDEV { get; set; } = "https://www.alphavantage.co/";
        public static string BaseUrlProd { get; set; } = "https://www.alphavantage.co/";
        public static string BaseUrlLocal { get; set; } = "https://www.alphavantage.co/";
        public static string BaseUrl { get; set; } = BaseUrlDEV;

        public static string ApiKey { get; set; } = "H3GS7S53733MO4W7";

        public static IMvxInteraction<bool> OnReadyForConversation => _readyForConversation;
        public static IMvxInteraction<string> OnTokenChange => _onTokenChange;

       
    
    }
}
