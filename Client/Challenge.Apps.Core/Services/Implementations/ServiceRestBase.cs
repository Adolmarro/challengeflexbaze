﻿using Challenge.Apps.Core.Http;
using Challenge.Apps.Core.Models;
using Challenge.Apps.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Challenge.Apps.Core.Services.Implementations
{
    class BaseService : ServiceRestBase<IBaseService>, IBaseService
    { 
        public async Task<UserAccesModel> Login(LoginDto loginDto, CancellationToken cancellationToken)
        {
            return await m_service.Login(loginDto, cancellationToken);
        }
    }
}
