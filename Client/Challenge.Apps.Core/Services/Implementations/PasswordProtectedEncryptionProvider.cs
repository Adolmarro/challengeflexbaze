﻿using Akavache;
using System;
using Challenge.Apps.Core.Services.Interfaces;
using System.IO;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Challenge.Apps.Core.Services.Implementations
{
    public sealed class PasswordProtectedEncryptionProvider : IPasswordProtectedEncryptionProvider
    {
        static byte[] salt;
        readonly IScheduler scheduler;
        readonly SymmetricAlgorithm symmetricAlgorithm;
        ICryptoTransform decryptTransform;
        ICryptoTransform encryptTransform;

        public PasswordProtectedEncryptionProvider(string pass1, string pass2)
        {
            scheduler = BlobCache.TaskpoolScheduler ?? throw new ArgumentNullException(nameof(scheduler), "Scheduler instance is null");
            salt = Encoding.ASCII.GetBytes("dVBZMQWyFRcJOIas");
            symmetricAlgorithm = new RijndaelManaged();
            var securePassword = pass1;
            SetPassword(securePassword);
        }
        public void SetPassword(string password)
        {
            if (password == null)
                throw new ArgumentNullException(nameof(password), "password can't be null");

            var derived = new Rfc2898DeriveBytes(password, salt);
            var bytesForKey = symmetricAlgorithm.KeySize / 8;
            var bytesForIV = symmetricAlgorithm.BlockSize / 8;
            symmetricAlgorithm.Key = derived.GetBytes(bytesForKey);
            symmetricAlgorithm.IV = derived.GetBytes(bytesForIV);
            decryptTransform = symmetricAlgorithm.CreateDecryptor(symmetricAlgorithm.Key, symmetricAlgorithm.IV);
            encryptTransform = symmetricAlgorithm.CreateEncryptor(symmetricAlgorithm.Key, symmetricAlgorithm.IV);
        }
        public IObservable<byte[]> DecryptBlock(byte[] block)
        {
            if (this.decryptTransform == null)
            {
                return Observable.Throw<byte[]>(new InvalidOperationException("You must call SetPassword first."));
            }
            return Observable
                .Start(
                    () => InMemoryTransform(block, this.decryptTransform),
                    this.scheduler);
        }
        public IObservable<byte[]> EncryptBlock(byte[] block)
        {
            if (this.encryptTransform == null)
            {
                return Observable.Throw<byte[]>(new InvalidOperationException("You must call SetPassword first."));
            }

            return Observable
                .Start(
                    () => InMemoryTransform(block, this.encryptTransform),
                    this.scheduler);
        }

        private static byte[] InMemoryTransform(byte[] bytesToTransform, ICryptoTransform transform)
        {
            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(bytesToTransform, 0, bytesToTransform.Length);
                }
                return memoryStream.ToArray();
            }
        }
    }
}
