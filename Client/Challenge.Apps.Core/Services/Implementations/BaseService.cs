﻿using Challenge.Apps.Core.Services.Interfaces;
using Refit;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace Challenge.Apps.Core.Services.Implementations
{
    public class ServiceRestBase<T> : IServiceRest
    {
        protected T m_service;
        public int CancelationTokenTimeOut { get; set; }
        public ServiceRestBase()
        {
            InitServiceRefit();
            Configuration.OnTokenChange.Requested += OnTokenChange_Requested;
        }

        private void OnTokenChange_Requested(object sender, MvvmCross.Base.MvxValueEventArgs<string> e)
        {
            InitServiceRefit();
        }

        public virtual void InitService()
        {
        }

        public void InitServiceRefit()
        {
            string UrlBase = Configuration.BaseUrl;

            CookieContainer cookieContainer = new CookieContainer()
            {
                MaxCookieSize = 81920
            };

            HttpClientHandler httpClientHandler = new HttpClientHandler
            {
                CookieContainer = cookieContainer,
                UseCookies = false
            };


            var client = new HttpClient(new HttpLoggingHandler(httpClientHandler))
            {
                BaseAddress = new Uri(UrlBase)
            };

            m_service = RestService.For<T>(client);
        }

    }
}
