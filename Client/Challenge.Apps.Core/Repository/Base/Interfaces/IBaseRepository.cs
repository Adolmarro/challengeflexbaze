﻿using MvvmCross.ViewModels;
using Refit;
using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.Apps.Core.Repository.Base.Interfaces
{
    public interface IBaseRepository
    {
        IMvxInteraction<Exception> Error { get; }
        IMvxInteraction<string> OnEmptyList { get; }
        void SetCancellationToken(int milliseconds);
    }
}
