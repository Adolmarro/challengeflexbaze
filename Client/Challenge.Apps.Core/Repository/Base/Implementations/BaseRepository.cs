﻿using Challenge.Apps.Core.Http;
using Challenge.Apps.Core.Models;
using Challenge.Apps.Core.Repository.Base.Interfaces;
using Challenge.Apps.Core.Services.Interfaces;
using MvvmCross.ViewModels;
using Refit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Challenge.Apps.Core.Repository.Base.Implementations
{
    public class BaseRepository : IBaseRepository
    {
        public MvxInteraction<Exception> _error = new MvxInteraction<Exception>();

        protected MvxInteraction<string> _onEmptyList = new MvxInteraction<string>();
        public int cancelationTokenTimeOut { get; set; }
        IBaseService _baseService;
        public BaseRepository(IBaseService serviceRest)
        {
            _baseService = serviceRest;
            SetCancellationToken(60000);
        }
        #region Interaction Event Getters
        public IMvxInteraction<string> OnEmptyList => _onEmptyList; //Event when some request have a list empty
        public IMvxInteraction<Exception> Error => _error;
        #endregion
        public CancellationToken GetCancellatioToken()
        {
            CancellationTokenSource tokenSource = new CancellationTokenSource();
            tokenSource.CancelAfter(cancelationTokenTimeOut);
            return tokenSource.Token;
        }
        public void SetCancellationToken(int milliseconds)
        {
            cancelationTokenTimeOut = milliseconds;
        }

    }
}
