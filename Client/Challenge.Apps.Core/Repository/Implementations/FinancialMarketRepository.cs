﻿using Challenge.Apps.Core.Models;
using Challenge.Apps.Core.Repository.Base.Implementations;
using Challenge.Apps.Core.Repository.Interfaces;
using Challenge.Apps.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Apps.Core.Repository.Implementations
{
    public class FinancialMarketRepository : BaseRepository, IFinancialMarketRepository
    {

        IFinancialMarketService _financialMarketService;
        public FinancialMarketRepository(IFinancialMarketService financialMarketService, IBaseService serviceRest) : base(serviceRest)
        {
            _financialMarketService = financialMarketService;
        }

        public async Task<QuoteModel> Quotes(string symbol, string apikey)
        {
            try
            {
                return await _financialMarketService.Quotes(symbol, apikey, GetCancellatioToken()); 
            }
            catch (Exception e)
            {
                _error.Raise(e);
                return null;
            }
        }

        public async Task<SearchModel> Search(string keywords, string apikey)
        {
            try
            {
                return await _financialMarketService.Search(keywords, apikey, GetCancellatioToken());
            }
            catch (Exception e)
            {
                _error.Raise(e);
                return null;
            }
        }
    }
}
