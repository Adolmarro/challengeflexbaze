﻿using Challenge.Apps.Core.Models;
using Challenge.Apps.Core.Repository.Base.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Apps.Core.Repository.Interfaces
{
    public interface IFinancialMarketRepository : IBaseRepository
    {
        Task<SearchModel> Search(string keywords, string apikey);

        Task<QuoteModel> Quotes(string symbol, string apikey);
    }
}
